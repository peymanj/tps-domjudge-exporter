#include "common.h"

int main(int argc, char* argv[]) {
	registerGen(argc, argv, 1);
	for (int i=1; i<argc; i++)
		U.push_back(argv[i]);
	int n = sz(U);
	fori(P, 1<<n) {
		vector<int> day;
		fori(i, n)
			if ((P>>i)&1)
				day.push_back(i);
		shuffle(day);
		days.push_back(day);
	}
	shuffle(days);
	print();
}
