// ... ... .. ....!
// ... ....... .... ...!

#include "common.h"
#include<bits/stdc++.h>
using namespace std;

#define rep(i, n) for(int i = 0, i##__n = (int)(n); i < i##__n; ++i)
#define fer(i, a, b) for(int i = (int)(a), i##__b = (int)(b); i < i##__b; ++i)
#define rof(i, b, a) for(int i = (int)(b), i##__a = (int)(a); i-- > i##__a; )
#define sz(x) (int((x).size()))
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define X first
#define Y second
//#define endl '\n'

template<class P, class Q> inline void smin(P &a, Q b) { if (b < a) a = b; }
template<class P, class Q> inline void smax(P &a, Q b) { if (a < b) a = b; }

typedef long long ll;
typedef pair<int, int> pii;

////////////////////////////////////////////////////////////////////////////////

const string alphabet = getDefaultCharset();

int main(int argc, char* argv[]) {
	ios_base::sync_with_stdio(false); cin.tie(0);
	registerGen(argc, argv, 1);

	int n = atoi(argv[1]);
	int a = atoi(argv[2]);
	int l = atoi(argv[3]);
	int k = atoi(argv[4]);

	rep(i, n) {
		string name;
		rep(j, l) name += alphabet[rnd.next(a)];
		U.push_back(name);
	}

	sort(U.begin(), U.end());
	U.erase(unique(U.begin(), U.end()), U.end());
	shuffle(U);

	rep(i, k) {
		vector<int> day;
		rep(j, U.size()) if(rnd.next(2))
			day.push_back(j);
		shuffle(day);
		days.push_back(day);
	}

	shuffle(days);
	print();
	return 0;
}

