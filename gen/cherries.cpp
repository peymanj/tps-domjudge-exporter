// ... ... .. ....!
// ... ....... .... ...!

#include "common.h"
#include<bits/stdc++.h>
using namespace std;

#define rep(i, n) for(int i = 0, i##__n = (int)(n); i < i##__n; ++i)
#define fer(i, a, b) for(int i = (int)(a), i##__b = (int)(b); i < i##__b; ++i)
#define rof(i, b, a) for(int i = (int)(b), i##__a = (int)(a); i-- > i##__a; )
#define sz(x) (int((x).size()))
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define X first
#define Y second
//#define endl '\n'

template<class P, class Q> inline void smin(P &a, Q b) { if (b < a) a = b; }
template<class P, class Q> inline void smax(P &a, Q b) { if (a < b) a = b; }

typedef long long ll;
typedef pair<int, int> pii;

////////////////////////////////////////////////////////////////////////////////

const string charset = getDefaultCharset();

int main(int argc, char* argv[]) {
	ios_base::sync_with_stdio(false); cin.tie(0);
	registerGen(argc, argv, 1);

	int n = atoi(argv[1]);
	int l = atoi(argv[2]);
	int k = atoi(argv[3]);

	rep(i, n / 4) {
		int nl = l / 3;
		string a = randStr(charset, nl);
		string b = randStr(charset, nl);
		string c = randStr(charset, nl);
		string d = randStr(charset, nl);
		string e = randStr(charset, nl);
		string f = randStr(charset, nl);
		string mid = randStr(charset, l - 2 * nl);
		U.push_back(a + mid + b);
		U.push_back(a + mid + c);
		U.push_back(e + mid + d);
		U.push_back(f + mid + d);
	}

	sort(U.begin(), U.end());
	U.erase(unique(U.begin(), U.end()), U.end());
	shuffle(U);

	addRandomDays(k);

	shuffle(days);
	print();

	return 0;
}

