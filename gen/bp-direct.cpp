#include "common.h"

int main(int argc, char* argv[]) {
	registerGen(argc, argv, 1);
	ensuref(argc >= 3 && argc%2 == 1, "invalid arg count %d", argc);
	int m = atoi(argv[1]);
	int n = atoi(argv[2]);
	vector<pair<int,int>> edges;
	for (int i=3; i<argc; i+=2) {
		int x = atoi(argv[i])-1;
		int y = atoi(argv[i+1])-1;
		edges.emplace_back(x, y);
	}
	simplePrintFromBipartite(m, n, edges, false);
}
