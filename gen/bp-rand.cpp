#include "common.h"

int main(int argc, char* argv[]) {
	registerGen(argc, argv, 1);
	int m = atoi(argv[1]);
	int n = atoi(argv[2]);
	int e = atoi(argv[3]);
	bool edgesAsSuperset = isTrue(argv[4]);
	vector<pair<int,int>> edges;
	fori(i, e)
		edges.emplace_back(rnd.next(m), rnd.next(n));
	simplePrintFromBipartite(m, n, edges, edgesAsSuperset);
}
