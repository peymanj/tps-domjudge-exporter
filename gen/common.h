#include "testlib.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
using namespace std;


#define tpc(T) template<class T>
tpc(T) inline int sz(const T& t) {
	return t.size();
}
#define fori(i, n) for (int i=0; i<(n); i++)
#define forv(i, v) fori(i, sz(v))
#define allOf(c) ((c).begin()), ((c).end())
#define spa << " " <<

tpc(T) inline ostream& operator<<(ostream& os, const vector<T>& r) {
	forv(i, r)
		os << (i?" ":"") << r[i];
	return os;
}

template<class A, class B> inline ostream& operator<<(ostream& os, const pair<A, B>& p) {
	return os << "(" << p.first << ", " << p.second << ")";
}

typedef vector<string> VS;

tpc(T) inline void shuffle(T& c) {
	shuffle(allOf(c));
}

tpc(T) inline T shuffled(T c) {
	shuffle(c);
	return c;
}


inline bool isTrue(string s) {
	forv(i, s)
		s[i] = tolower(s[i]);
	if (s=="1" || s=="t" || s=="true")
		return true;
	if (s=="0" || s=="f" || s=="false")
		return false;
	quitf(_fail, "Invalid bool string '%s'", s.c_str());
}

VS U;
vector<vector<int>> days;

const int max_days = 100;

NORETURN inline void print() {
	cout << sz(U) spa sz(days) << endl;
	cout << U << endl;
	for (auto& day : days) {
		cout << sz(day);
		for (int x : day)
			cout spa x+1;
		cout << endl;
	}
	exit(0);
}


inline void addRandomDays(int d, vector<int> superset) {
	fori(i0, d) {
		int prob = (i0 == 0 ? 100 : rnd.next(1, 99));
		vector<int> day;
		for (int i : superset)
			if (rnd.next(100) < prob)
				day.push_back(i);
		shuffle(day);
		days.push_back(day);
	}
}

inline void addRandomDays(int d) {
	vector<int> v(U.size());
	iota(allOf(v), 0);
	addRandomDays(d, v);
}



inline string randStr(const string& charset, int len) {
	string s = "";
	fori(i, len)
		s += rnd.any(charset);
	return s;
}


inline string getDefaultCharset() {
	string charset;
	for (char c = 'A'; c <= 'Z'; c++)
		charset += c;
	for (char c = '0'; c <= '9'; c++)
		charset += c;
	charset += '-';
	return charset;
}

inline pair<VS, VS> _bpVertexStrings(int m, string charset) {
	int d = sz(charset);
	VS par = {""};
	while (sz(par)*(d-1) < m) {
		VS ver;
		for (string p : par)
			for (char c : shuffled(charset))
				ver.push_back(p+c);
		par.swap(ver);
	}
	pair<VS, VS> res;
	for (string p : par) {
		string scs = shuffled(charset);
		for (char c : scs) {
			if (sz(res.first) >= m)
				break;
			string s = p+c;
			if (c == scs[0])
				res.second.push_back(s);
			else
				res.first.push_back(s);
		}
	}
	shuffle(res.first);
	shuffle(res.second);
	ensuref(sz(res.first) == m, "BUG: sz(res.first) != m");
	//cerr << res << endl;
	return res;
}

inline void makeFromBipartite(int m, int n, const vector<pair<int,int>> edges, int len, const string& charset) {
	for (auto e : edges) {
		ensuref(0 <= e.first && e.first < m, "invalid e.first: %d, m=%d", e.first, m);
		ensuref(0 <= e.second && e.second < n, "invalid e.second: %d, n=%d", e.second, n);
	}
	pair<VS, VS> a, b;
	a = _bpVertexStrings(m, charset);
	b = _bpVertexStrings(n, charset);
	for (VS* vs : {&b.first, &b.second})
		for (string& s : *vs)
			reverse(allOf(s));
	auto makeEdgeStr = ([len, charset](string x, string y) {
		const string midStr = randStr(charset, max(0, len-sz(x)-sz(y)));
		return x+midStr+y;
	});
	vector<pair<string, bool>> markedStrs;
	for (auto e : edges)
		markedStrs.emplace_back(makeEdgeStr(a.first[e.first], b.first[e.second]), true);
	{// Adding dummy strings to prevent the selection of parent trie nodes
		int maxs = max(sz(a.second), sz(b.second));
		fori(i, maxs) {
			int ai = (i < sz(a.second)) ? i : rnd.next(sz(a.second));
			int bi = (i < sz(b.second)) ? i : rnd.next(sz(b.second));
			markedStrs.emplace_back(makeEdgeStr(a.second[ai], b.second[bi]), false);
		}
	}
	shuffle(markedStrs);
	// cerr << edges << endl;
	// cerr << markedStrs << endl;
	vector<int> mainDay;
	forv(i, markedStrs) {
		auto& markedStr = markedStrs[i];
		U.push_back(markedStr.first);
		if (markedStr.second)
			mainDay.push_back(i);
	}
	shuffle(mainDay);
	days.push_back(mainDay);
}

NORETURN inline void simplePrintFromBipartite(int m, int n, const vector<pair<int,int>> edges, bool edgesAsSuperset) {
	makeFromBipartite(m, n, edges, 10, getDefaultCharset());
	vector<int> superset;
	if (edgesAsSuperset)
		superset = days[0];
	else {
		forv(i, U)
			superset.push_back(i);
	}
	addRandomDays(max_days-1, superset);
	shuffle(days);
	print();
}
