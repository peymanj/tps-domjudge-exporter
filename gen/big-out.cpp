#include "common.h"

int main(int argc, char* argv[]) {
	registerGen(argc, argv, 1);
	int m = atoi(argv[1]);
	int l = atoi(argv[2]);
	string charset = getDefaultCharset();

	auto p = _bpVertexStrings(m, charset);

	for (VS* vs : {&p.first, &p.second})
		for (string& s : *vs) {
			string t = s.substr(0, sz(s)-1);
			reverse(allOf(t));
			s += t;
		}

	int pl = sz(p.first[0]);
	int h = max(l-pl, 0)/2;
	string prefix = randStr(charset, h);
	string suffix = randStr(charset, l-pl-h);
	for (VS* vs : {&p.first, &p.second})
		for (string& s : *vs)
			s = prefix + s + suffix;

	vector<pair<string, bool>> markedStrs;
	for (string s : p.first)
		markedStrs.emplace_back(s, true);
	// Adding dummy strings to prevent the selection of parent trie nodes
	for (string s : p.second)
		markedStrs.emplace_back(s, false);
	shuffle(markedStrs);
	// cerr << markedStrs << endl;
	vector<int> mainDay;
	forv(i, markedStrs) {
		auto& markedStr = markedStrs[i];
		U.push_back(markedStr.first);
		if (markedStr.second)
			mainDay.push_back(i);
	}
	shuffle(mainDay);
	days.push_back(mainDay);
	vector<int> superset;
	forv(i, U)
		superset.push_back(i);
	addRandomDays(max_days-1, superset);
	shuffle(days);
	print();
}
