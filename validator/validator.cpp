// Validator for problem: wildcards
//Author: Mahdi Safarnejad

#include "testlib.h"
#include <set>
using namespace std;

#define forn(i, n) for (int i = 0; i < int(n); i++)

int main(int argc, char* argv[]) {
    registerValidation(argc, argv);
    int n = inf.readInt(1, 1000, "n");
    inf.readSpace();
    int k = inf.readInt(1, 100, "k");
    inf.readEoln();
    forn(i, n) {
        if (i > 0) inf.readSpace();
        inf.readToken("[A-Z0-9-]{1,10}", "name_i");
    }
    inf.readEoln();
    forn(i, k) {
        int s_i = inf.readInt(0, n, "s_i");
        set<int> s;
        forn(j, s_i) {
            inf.readSpace();
            int index = inf.readInt(1, n, "s_i_j");
            s.insert(index);
        }
        inf.readEoln();
        if ((int) s.size() != s_i) quit(_fail, "Duplicate number in S_" + toString(i));
    }
    inf.readEof();
    return 0;
}
