// Validator for problem: wildcards
//Author: Kian Mirjalali
#include "testlib.h"
using namespace std;

#define tpc(T) template<class T>
tpc(T) inline int sz(const T& t) {
	return t.size();
}
#define allOf(c) ((c).begin()), ((c).end())
#define fori(i, n) for (int i=0; i<(n); i++)

tpc(T) bool isUnique(vector<T> v) {
	sort(allOf(v));
	return unique(allOf(v)) == v.end();
}


int main(int argc, char* argv[]) {
	registerValidation(argc, argv);
	int n = inf.readInt(1, 1000, "n");
	inf.readSpace();
	int k = inf.readInt(1, 100, "k");
	inf.readEoln();
	vector<string> names = inf.readTokens(n, "[A-Z0-9-]{1,10}", "name");
	inf.readEoln();
	ensuref(isUnique(names), "names are not unique");
	fori(i, k) {
		int sSize = inf.readInt(0, n, "S size");
		vector<int> S;
		if (sSize > 0)
			inf.readSpace();
		S = inf.readInts(sSize, 1, n, "S members");
		inf.readEoln();
		ensuref(isUnique(S), "S members are not unique");
	}
	inf.readEof();
	return 0;
}
