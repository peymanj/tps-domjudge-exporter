//Checker for Problem: Wildcards
//Author: Kian Mirjalali
#include "testlib.h"
#include <vector>
#include <algorithm>
#include <string>
using namespace std;

#define tpc(T) template<class T>
tpc(T) inline int sz(const T& t) {
	return t.size();
}
#define fori(i, n) for (int i=0; i<(n); i++)
#define forv(i, v) fori(i, sz(v))
#define allOf(c) ((c).begin()), ((c).end())
#define dbg(x) #x << "=" << x
#define show(x) cerr << dbg(x) << endl

tpc(T) inline ostream& operator<<(ostream& os, const vector<T>& r) {
	forv(i, r)
		os << (i?" ":"") << r[i];
	return os;
}


class ExactMatcher {
	set<string> ss;
public:
	inline void add(string p) {
		ss.insert(p);
	}
	inline bool matches(string s) const {
		return ss.count(s) > 0;
	}
};

class PrefixMatcher {
	set<string> ss;
public:
	inline void add(string p) {
		ss.insert(p);
	}
	inline bool matches(string s) const {
		string t = "";
		fori(i, sz(s)+1) {
			if (i > 0)
				t += s[i-1];
			if (ss.count(t) > 0)
				return true;
		}
		return false;
	}
};

class SuffixMatcher {
	PrefixMatcher pm;
public:
	inline void add(string p) {
		reverse(allOf(p));
		pm.add(p);
	}
	inline bool matches(string s) const {
		reverse(allOf(s));
		return pm.matches(s);
	}
};

class MultiMatcher {
	ExactMatcher exactMatcher;
	PrefixMatcher prefixMatcher;
	SuffixMatcher suffixMatcher;

public:
	inline void addExact(string p) {
		//cerr << "add exact '" << p << "'" << endl;
		exactMatcher.add(p);
	}
	inline void addPrefix(string p) {
		//cerr << "add prefix '" << p << "'" << endl;
		prefixMatcher.add(p);
	}
	inline void addSuffix(string p) {
		//cerr << "add suffix '" << p << "'" << endl;
		suffixMatcher.add(p);
	}
	inline bool matches(string s) const {
		bool result = exactMatcher.matches(s) || prefixMatcher.matches(s) || suffixMatcher.matches(s);
		//cerr << "matches '" << s << "' -> " << result << endl;
		return result;
	}
};


int main(int argc, char * argv[]) {
	registerChecker("wildcards", argc, argv);

	int n = inf.readInt();
	int k = inf.readInt();
	vector<string> U = inf.readTokens(n);

	for (int day = 1; day <= k; day++) {
		int outCost = ouf.readInt();
		int outSize = ouf.readInt();
		vector<string> P = ouf.readTokens(outSize);
		for (string& p : P) {
			forv(i, p)
				p[i] = toupper(p[i]);
		}
		MultiMatcher P_matcher;
		for (string p : P) {
			int sc = count(allOf(p), '*');
			quitif(sc > 1, _wa, "day %d: too many stars in the pattern '%s'", day, p.c_str());
			if (sc == 0)
				P_matcher.addExact(p);
			else {
				// sc == 1
				if (p.back() == '*')
					P_matcher.addPrefix(p.substr(0, sz(p)-1));
				else if (p.front() == '*')
					P_matcher.addSuffix(p.substr(1));
				else
					quitf(_wa, "day %d: wrong position of '*' in the pattern '%s'", day, p.c_str());
			}
		}
		vector<bool> inS(n, false);
		int sSize = inf.readInt();
		vector<int> S = inf.readInts(sSize, 1, n);
		for (int x : S)
			inS[x-1] = true;
		//show(inS);
		forv(i, U) {
			if (P_matcher.matches(U[i]))
				quitif(!inS[i], _wa, "day %d: patterns cover non-member element '%s'", day, U[i].c_str());
			else
				quitif(inS[i], _wa, "day %d: patterns do not cover member element '%s'", day, U[i].c_str());
		}
		int compCost = sz(S)*1000 + sz(P);
		quitif(compCost != outCost, _wa, "day %d: wrong computation of cost %d != %d", day, outCost, compCost);
		int ansCost = ans.readInt();
		ans.readLine();
		//show(ansCost);
		quitif(ansCost < outCost, _wa, "day %d: too much cost %d > %d", day, outCost, ansCost);
		quitif(ansCost > outCost, _fail, "day %d: better than optimum cost %d < %d", day, outCost, ansCost);
	}
	ouf.skipBlanks();
	ouf.readEof();
	quit(_ok);
}
