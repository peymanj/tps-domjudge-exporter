// ... ... .. ....!
// ... ....... .... ...!

#include<bits/stdc++.h>
using namespace std;

#define rep(i, n) for(int i = 0, i##__n = (int)(n); i < i##__n; ++i)
#define fer(i, a, b) for(int i = (int)(a), i##__b = (int)(b); i < i##__b; ++i)
#define rof(i, b, a) for(int i = (int)(b), i##__a = (int)(a); i-- > i##__a; )
#define sz(x) (int((x).size()))
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define X first
#define Y second
//#define endl '\n'

template<class P, class Q> inline void smin(P &a, Q b) { if (b < a) a = b; }
template<class P, class Q> inline void smax(P &a, Q b) { if (a < b) a = b; }

typedef long long ll;
typedef pair<int, int> pii;
typedef pair<string, int> psi;

////////////////////////////////////////////////////////////////////////////////

const int PCOST = 1000;
const int maxn = 1000 + 10;

int n, k;
string str[maxn];
bool mark[maxn];
int edge[maxn];

struct SDS {
	psi s[maxn];
	int c[maxn + 1];
	vector<pair<pii, vector<int>>> nodes;

	void init() {
		sort(s, s + n);
		c[0] = 0;
		fer(i, 1, n) 
			c[i] = mismatch(all(s[i].X), all(s[i-1].X)).first - s[i].X.begin();
		c[n] = 0;
	}

	void make_nodes(int l, int r) {
		while(l < r) {
			if(c[l] >= c[r]) {
				nodes.pb({{l, c[l] + 1}, vector<int>()});
				int lim = c[l];
				nodes.back().Y.pb(s[l].Y);
				for(l++; c[l] > lim; l++) nodes.back().Y.pb(s[l].Y);
			} else {
				nodes.pb({{r-1, c[r] + 1}, vector<int>()});
				int lim = c[r];
				nodes.back().Y.pb(s[r-1].Y);
				for(r--; c[r] > lim; r--) nodes.back().Y.pb(s[r-1].Y);
			}
		}
		assert(l == r);
	}

	void make_graph() {
		nodes.clear();
		for(int i = 0; i < n; i++) {
			if(!mark[s[i].Y]) continue;
			int j = i+1;
			while(j < n && mark[s[j].Y]) j++;
			make_nodes(i, j);
			i = j;
		}
		rep(u, sz(nodes))
			for(int e: nodes[u].Y)
				edge[e] ^= u;
	}
} d[2];

bool vis[maxn];
int l_match[maxn], r_match[maxn];

bool dfs(int u) {
	if(u == -1) return true;
	for(int e: d[0].nodes[u].Y) {
		int v = edge[e] ^ u;
		if(!vis[v]) {
			vis[v] = true;
			if(dfs(r_match[v])) {
				l_match[u] = v;
				r_match[v] = u;
				return true;
			}
		}
	}
	return false;
}

vector<pii> matching() {
	memset(l_match, -1, sizeof l_match);
	memset(r_match, -1, sizeof r_match);
	rep(u, sz(d[0].nodes)) {
		memset(vis, false, sizeof vis);
		dfs(u);
	}
	
//	memset(vis, false, sizeof vis);
	rep(u, sz(d[0].nodes))
		if(l_match[u] == -1)
			dfs(u);

	vector<pii> res;
	rep(u, sz(d[0].nodes)) if(l_match[u] != -1) {
		if(vis[l_match[u]])
			res.pb({1, l_match[u]});
		else
			res.pb({0, u});
	}

	return res;
}

int main() {
	ios_base::sync_with_stdio(false); cin.tie(0);

	cin >> n >> k;
	rep(i, n) cin >> str[i];

	rep(i, n) {
		d[0].s[i] = psi(str[i], i);
		d[1].s[i] = psi(str[i], i);
		reverse(all(d[1].s[i].X));
	}

	rep(i, 2) d[i].init();

	rep(tt, k) {
		int t; cin >> t;
		memset(mark, false, sizeof mark);
		rep(i, t) {
			int x; cin >> x; x--;
			mark[x] = true;
		}
		
		if(t == n) {
			cout << t * PCOST + 1 << ' ' << 1 << ' ' << "*" << endl;
			continue;
		}

		memset(edge, 0, sizeof edge);
		rep(i, 2) d[i].make_graph();

		vector<pii> ans = matching();

		int cost = t * PCOST + sz(ans);
		cout << cost << ' ' << sz(ans);
		for(pii p: ans) {
			string w = d[p.X].s[d[p.X].nodes[p.Y].X.X].X;
			int len = d[p.X].nodes[p.Y].X.Y;
			if(len <= sz(w)) w = w.substr(0, len) + "*";
			if(p.X) reverse(all(w));
			cout << ' ' << w;
		}
		cout << endl;
	}

	return 0;
}

