
# This is the template for checking a solution output
#  assuming the solution is run in the sandbox against a testcase.

set -euo pipefail

# Testcase name (provided to be used, just in case!)
test_name="$1"

# Location of the input file
input="$2"

# Location of judge answer file
judge_answer="$3"

# Location of solution standard output file
sol_stdout="$4"

# Location of solution standard error file
sol_stderr="$5"

if "${HAS_CHECKER}"; then
	feedback_dir="${SANDBOX}/checker_feedback_${test_name}"
	rm -rf "${feedback_dir}"
	mkdir "${feedback_dir}"

	"${CHECKER_DIR}/checker.exe" "${input}" "${judge_answer}" "${feedback_dir}/" < "${sol_stdout}" || ret=$?

	if [ ${ret} -eq 42 ]; then
	    echo "Correct"
	    echo 1
	elif [ ${ret} -eq 43 ]; then
	    echo "Wrong Answer"
	    echo 0
	else
	    echo "Judge Failure; Contact staff!"
	    echo 0
	fi

	judge_message_file="${feedback_dir}/judgemessage.txt"
	if [ -f "${judge_message_file}" ]; then
	    cat "${judge_message_file}"
	else
	    echo
	fi
elif "${HAS_MANAGER}"; then
	# If there is no checker, then the manager outputs should be in the format of checker outputs.
	# TODO
	echo "Judge Failure; Contact staff!"
	echo "0"
	echo "Using manager as checker is not implemented."
else
	# There is no checker or manager. Comparing solution standard output with judge answer file.
	# Not using test_name & input & sol_stderr
	DIFF="diff"
	DIFF_FLAGS="-bq"
	if ! command -v "${DIFF}" >/dev/null 2>&1 ; then
		echo "Judge Failure; Contact staff!"
		echo "0"
		echo "Command '${DIFF}' not found."
	elif "${DIFF}" "${DIFF_FLAGS}" "${judge_answer}" "${sol_stdout}" >/dev/null; then
		echo "Correct"
		echo "1"
		echo ""
	else
		echo "Wrong Answer"
		echo "0"
		echo "The output differs from the correct answer."
	fi
fi

