//Problem: wildcards
//Author: Kian Mirjalali
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>
#include <string>
#include <cstdarg>
#include <cstdlib>
using namespace std;


#define tpc(T) template<class T>
tpc(T) inline int sz(const T& t) {
	return t.size();
}
#define fori(i, n) for (int i=0; i<(n); i++)
#define forv(i, v) fori(i, sz(v))
#define allOf(c) ((c).begin()), ((c).end())
#define spa << " " <<
#define dbg(x) #x << "=" << x
#define show(x) cerr << dbg(x) << endl

tpc(T) inline T read(istream& is=cin) {
	T x;
	is >> x;
	return x;
}

tpc(T) inline ostream& operator<<(ostream& os, const vector<T>& r) {
	forv(i, r)
		os << (i?" ":"") << r[i];
	return os;
}

template<class A, class B> inline ostream& operator<<(ostream& os, const pair<A, B>& p) {
	return os << "(" << p.first << ", " << p.second << ")";
}


typedef pair<int, int> PII;
typedef vector<int> VI;


#ifdef _MSC_VER
# define NORETURN __declspec(noreturn)
#elif defined __GNUC__
# define NORETURN __attribute__ ((noreturn))
#else
# define NORETURN
#endif


static char __format_buffer[10000000];
#define FMT_TO_RESULT(fmt, cstr, result)  std::string result;              \
            va_list ap;                                                    \
            va_start(ap, fmt);                                             \
            vsnprintf(__format_buffer, sizeof(__format_buffer), cstr, ap); \
            va_end(ap);                                                    \
            __format_buffer[sizeof(__format_buffer) - 1] = 0;              \
            result = std::string(__format_buffer);                         \

NORETURN void error(int exitCode, string message) {
	cerr << message << endl;
	exit(exitCode);
}

#ifdef __GNUC__
__attribute__ ((format (printf, 2, 3)))
#endif
NORETURN void error(int exitCode, const char* format, ...) {
	FMT_TO_RESULT(format, format, message);
	error(exitCode, message);
}

#ifdef __GNUC__
__attribute__ ((format (printf, 3, 4)))
#endif
void errorif(bool cond, int exitCode, const char* format, ...) {
	if (cond) {
		FMT_TO_RESULT(format, format, message);
		error(exitCode, message);
	}
}


class RangeGenerator {
	int _index, _step, _end;
public:
	inline RangeGenerator(int start, int end, int step):
			_index(start-step), _step(step), _end(end) {}

	inline bool hasNext() const {
		int nxt = _index + _step;
		if (_step > 0)
			return nxt < _end;
		else
			return nxt > _end;
	}

	inline int next() {
		errorif(!hasNext(), 104, "Illegal state; next() called when hasNext() is false");
		return _index += _step;
	}
};


class StringIterator {
	const string _str;
	RangeGenerator _rangeGenerator;
public:
	inline StringIterator(const string& str, const RangeGenerator &rangeGenerator):
			_str(str), _rangeGenerator(rangeGenerator) {}

	inline bool hasNext() const {
		return _rangeGenerator.hasNext();
	}

	inline char next() {
		return _str[_rangeGenerator.next()];
	}
};



tpc(D) struct AbstractTrie {
	struct Node;
	typedef Node* NodePtr;
	typedef const Node* ConstNodePtr;
	struct Node {
		D _data;
		vector<NodePtr> children;

		~Node() {
			for (NodePtr child : children)
				if (child != nullptr)
					delete child;
		}
	};

private:
	string _charset;
	VI charIndex;
	char endingChar;
	int endingIndex;
	int childrenSize;
	Node root;

	inline int getCharIndex(char c) const {
		int idx = charIndex[int(c)];
		errorif(idx == -1, 101, "Invalid character '%c'", c);
		return idx;
	}

	class StringCharIndexIterator {
		const AbstractTrie<D>* _parentInstance;
		StringIterator _stringIterator;
		bool _includeEnding;
		bool usedEnding;
	public:
		inline StringCharIndexIterator(const AbstractTrie<D>* parentInstance, StringIterator stringIterator, bool includeEnding):
				_parentInstance(parentInstance), _stringIterator(stringIterator), _includeEnding(includeEnding), usedEnding(false) {}

		inline bool hasNext() const {
			return !usedEnding || _stringIterator.hasNext();
		}

		inline int next() {
			if (_stringIterator.hasNext())
				return _parentInstance->getCharIndex(_stringIterator.next());
			errorif(usedEnding, 104, "Illegal state; next() called when hasNext() is false (usedEnding)");
			usedEnding = true;
			return _parentInstance->endingIndex;
		}
	};

	inline NodePtr getChild(NodePtr node, int idx) const {
		return node->children.empty() ? nullptr : node->children[idx];
	}
	inline ConstNodePtr getChild(ConstNodePtr node, int idx) const {
		return node->children.empty() ? nullptr : node->children[idx];
	}

	inline NodePtr getChild(NodePtr node, char c) const {
		return getChild(node, getCharIndex(c));
	}
	inline ConstNodePtr getChild(ConstNodePtr node, char c) const {
		return getChild(node, getCharIndex(c));
	}

	inline NodePtr getEndingChild(NodePtr node) const {
		return getChild(node, endingIndex);
	}
	inline ConstNodePtr getEndingChild(ConstNodePtr node) const {
		return getChild(node, endingIndex);
	}

	inline NodePtr getCreatedChild(NodePtr node, int idx) {
		if (node->children.empty())
			node->children.resize(childrenSize, nullptr);
		if (node->children[idx] == nullptr)
			node->children[idx] = new Node();
		return node->children[idx];
	}

	inline NodePtr getCreatedChild(NodePtr node, char c) {
		return getCreatedChild(node, getCharIndex(c));
	}

	inline StringIterator getStringIterator(const string& s) const {
		return StringIterator(s, getIndexGenerator(sz(s)));
	}

	inline StringCharIndexIterator getStringCharIndexIterator(const string& s, bool ending) const {
		return StringCharIndexIterator(this, getStringIterator(s), ending);
	}

	inline NodePtr getNode(const string& s, bool ending) {
		NodePtr currNode = &root;
		for (StringCharIndexIterator sciit = getStringCharIndexIterator(s, ending); currNode != nullptr && sciit.hasNext(); )
			currNode = getChild(currNode, sciit.next());
		return currNode;
	}

	inline ConstNodePtr getNode(const string& s, bool ending) const {
		ConstNodePtr currNode = &root;
		for (StringCharIndexIterator sciit = getStringCharIndexIterator(s, ending); currNode != nullptr && sciit.hasNext(); )
			currNode = getChild(currNode, sciit.next());
		return currNode;
	}

	inline NodePtr getCreatedNode(const string& s, bool ending) {
		NodePtr currNode = &root;
		for (StringCharIndexIterator sciit = getStringCharIndexIterator(s, ending); sciit.hasNext(); )
			currNode = getCreatedChild(currNode, sciit.next());
		return currNode;
	}

	void traverseSubtree(NodePtr node, function<void(NodePtr)> preFunc, function<void(NodePtr)> postFunc) {
		if (node == nullptr)
			return;
		preFunc(node);
		for (NodePtr child : node->children)
			traverseSubtree(child, preFunc, postFunc);
		postFunc(node);
	}

	void printNode(ostream& os, ConstNodePtr node, const string& path) const {
		if (node == nullptr)
			return;
		os << path spa node->_data << endl;
		forv(i, node->children)
			printNode(os, node->children[i], path+((i<sz(_charset)) ? _charset[i] : endingChar));
	}

	inline void print(ostream& os) const {
		printNode(os, &root, "");
	}

	tpc(D2) friend inline ostream& operator<<(ostream& os, const AbstractTrie<D2>& t) {
		t.print(os);
		return os;
	}

protected:
	virtual ~AbstractTrie() = default;
	virtual RangeGenerator getIndexGenerator(int len) const = 0;

public:
	inline AbstractTrie(const string& charset, char endingChar_='$'): _charset(charset), charIndex(256, -1),
			endingChar(endingChar_), endingIndex(sz(charset)), childrenSize(sz(charset)+1) {
		forv(i, charset)
			charIndex[int(charset[i])] = i;
	}

	inline int getEndingIndex() const {
		return endingIndex;
	}

	inline void add(const string& s) {
		getCreatedNode(s, true);
	}

	inline D& dataAt(const string& s, bool ending) {
		NodePtr node = getNode(s, ending);
		errorif(node == nullptr, 102, "Invalid string '%s'", s.c_str());
		return node->_data;
	}

	inline void traverse(function<void(NodePtr)> preFunc, function<void(NodePtr)> postFunc) {
		traverseSubtree(&root, preFunc, postFunc);
	}

	inline void traversePreOrder(function<void(NodePtr)> func) {
		traverse(func, [](auto) {});
	}

	inline void traversePostOrder(function<void(NodePtr)> func) {
		traverse([](auto) {}, func);
	}

	inline void resetAllData(const D& data) {
		traversePreOrder([data](NodePtr node){ node->_data = data; });
	}

	inline pair<NodePtr, int> findNode(const string& s, bool ending, function<bool(NodePtr)> predicateFunc) {
		int strIndex = 0;
		StringCharIndexIterator sciit = getStringCharIndexIterator(s, ending);
		for (NodePtr currNode = &root; currNode != nullptr; ) {
			if (predicateFunc(currNode))
				return make_pair(currNode, strIndex);
			if (!sciit.hasNext())
				break;
			strIndex++;
			currNode = getChild(currNode, sciit.next());
		}
		return make_pair(nullptr, -1);
	}
};


tpc(D) class Trie: public AbstractTrie<D> {
protected:
	virtual RangeGenerator getIndexGenerator(int len) const {
		return RangeGenerator(0, len, 1);
	}
public:
	inline Trie(string charset): AbstractTrie<D>(charset) {}
	inline Trie(string charset, char endingChar): AbstractTrie<D>(charset, endingChar) {}
	virtual ~Trie() = default;
};


tpc(D) class ReversedTrie: public AbstractTrie<D> {
protected:
	virtual RangeGenerator getIndexGenerator(int len) const {
		return RangeGenerator(len-1, -1, -1);
	}
public:
	inline ReversedTrie(string charset): AbstractTrie<D>(charset) {}
	inline ReversedTrie(string charset, char endingChar): AbstractTrie<D>(charset, endingChar) {}
	virtual ~ReversedTrie() = default;
};


tpc(T) class ObjectIndexer {
	vector<T> objects;
	map<T, int> indices;
public:
	inline int size() const {
		return sz(objects);
	}

	inline T getObject(int index) const {
		return objects[index];
	}

	inline int getIndex(const T& obj) {
		auto it = indices.find(obj);
		if (it != indices.end())
			return it->second;
		objects.push_back(obj);
		return indices[obj] = sz(objects)-1;
	}
};


class BipartiteMatchingSolver {
	int nX, nY;
	vector<VI> adjX, adjY;
	vector<bool> markX;
	VI matchX, matchY;

	bool dfs(int x) {
		markX[x]=true;
		for (int y : adjX[x])
			if (matchY[y]<0 || (!markX[matchY[y]] && dfs(matchY[y]))) {
				matchX[x] = y;
				matchY[y] = x;
				return true;
			}
		return false;
	}

	inline void resetMark() {
		fill(allOf(markX), false);
	}

	inline void matching() {
		resetMark();
		fori(x, nX)
			if (matchX[x]<0 && dfs(x))
				resetMark();
	}

public:
	inline BipartiteMatchingSolver(int m, int n): nX(m), nY(n),
			adjX(m), adjY(n), markX(m), matchX(m), matchY(n) {}

	inline void addEdge(int x, int y) {
		adjX[x].push_back(y);
		adjY[y].push_back(x);
	}

	inline void solve() {
		fill(allOf(matchX), -1);
		fill(allOf(matchY), -1);

		matching();
		//run again!
		matching();
	}

	inline vector<PII> getMaxMatching() const {
		vector<PII> match;
		fori(x, nX)
			if (matchX[x] >= 0)
				match.emplace_back(x, matchX[x]);
		return match;
	}

	inline vector<PII> getMinEdgeCover() const {
		vector<PII> ec;
		fori(x, nX)
			if (matchX[x] >= 0)
				ec.emplace_back(x, matchX[x]);
			else
				if (!adjX[x].empty())
					ec.emplace_back(x, adjX[x].back());
		fori(y, nY)
			if (matchY[y] < 0 && !adjY[y].empty())
				ec.emplace_back(adjY[y].back(), y);
		return ec;
	}

	inline pair<VI, VI> getMinVertexCover() const {
		pair<VI, VI> vc;
		fori(x, nX)
			if (!markX[x])
				vc.first.push_back(x);
		fori(y, nY)
			if (matchY[y]>=0 && markX[matchY[y]])
				vc.second.push_back(y);
		return vc;
	}

	inline pair<VI, VI> getMaxIndependentSet() const {
		pair<VI, VI> is;
		fori(x, nX)
			if (markX[x])
				is.first.push_back(x);
		fori(y, nY)
			if (!(matchY[y]>=0 && markX[matchY[y]]))
				is.second.push_back(y);
		return is;
	}
};

int main() {
	string charset;
	for (char c = 'A'; c <= 'Z'; c++)
		charset += c;
	for (char c = '0'; c <= '9'; c++)
		charset += c;
	charset += '-';

	int n, k;
	cin >> n >> k;
	vector<string> U(n);
	Trie<bool> forwardTrie(charset);
	ReversedTrie<bool> backwardTrie(charset);
	vector<AbstractTrie<bool>*> tries {&forwardTrie, &backwardTrie};
	#define forTries for (auto trie : tries)
	fori(i, n) {
		cin >> U[i];
		forTries trie->add(U[i]);
	}

	for (int day = 1; day <= k; day++) {
		forTries trie->resetAllData(false);
		vector<string> S;
		{
			int ss = read<int>();
			// show(ss);
			fori(i0, ss) {
				int si = read<int>()-1;
				errorif(si < 0 || n <= si, 103, "day %d: invalid name index: %d", day, si);
				S.push_back(U[si]);
			}
		}
		for (string name : S)
			forTries trie->dataAt(name, true) = true;
		// forTries cerr << *trie << endl;
		forTries trie->traversePostOrder([](auto node) {
			if (node->children.empty())
				return;
			node->_data = true;
			for (auto child: node->children)
				if (child != nullptr)
					node->_data &= child->_data;
			if (node->_data)
				for (auto child: node->children)
					if (child != nullptr)
						child->_data = false;
		});
		forTries trie->traversePreOrder([&trie](auto node) {
			if (!node->_data || node->children.empty())
				return;
			const int ei = trie->getEndingIndex();
			forv(i, node->children)
				if ((i == ei) ^ (node->children[i] != nullptr))
					return;
			node->_data = false;
			node->children[ei]->_data = true;
		});
		// show(U);
		// show(S);
		// forTries cerr << *trie << endl;
		ObjectIndexer<string> forwardStringIndexer, backwardStringIndexer;
		vector<PII> edges;
		for (string name : S) {
			auto forwardPair = forwardTrie.findNode(name, true, [](auto node){ return node->_data; });
			errorif(forwardPair.first == nullptr, 107, "forward node for '%s' is null", name.c_str());
			string forwardStr = (forwardPair.second <= sz(name)) ? name.substr(0, forwardPair.second)+"*" : name;
			int forwardNodeIndex = forwardStringIndexer.getIndex(forwardStr);
			auto backwardPair = backwardTrie.findNode(name, true, [](auto node){ return node->_data; });
			errorif(backwardPair.first == nullptr, 107, "backward node for '%s' is null", name.c_str());
			string backwardStr = (backwardPair.second <= sz(name)) ? "*"+name.substr(sz(name)-backwardPair.second) : name;
			int backwardNodeIndex = backwardStringIndexer.getIndex(backwardStr);
			// cerr << name spa forwardStr spa backwardStr spa forwardNodeIndex spa backwardNodeIndex << endl;
			edges.emplace_back(forwardNodeIndex, backwardNodeIndex);
		}
		BipartiteMatchingSolver mSolver(sz(forwardStringIndexer), sz(backwardStringIndexer));
		// cerr << "bp " << sz(forwardStringIndexer) spa sz(backwardStringIndexer) << endl << edges << endl;
		for (auto e : edges)
			mSolver.addEdge(e.first, e.second);
		mSolver.solve();
		auto vc = mSolver.getMinVertexCover();
		vector<string> srep;
		for (int fi : vc.first)
			srep.push_back(forwardStringIndexer.getObject(fi));
		for (int bi : vc.second)
			srep.push_back(backwardStringIndexer.getObject(bi));

		cout << sz(S)*1000+sz(srep) spa sz(srep) << (sz(srep)?" ":"") << srep << endl;
		// cerr << sz(S)*1000+sz(srep) spa sz(srep) << (sz(srep)?" ":"") << srep << endl;
		// cerr << "------------" << endl;
	}
	return 0;
}
