
//be naame khodaa
#include <iostream>
#include <cmath>
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <string>
#include <cstdio>
#include <algorithm>
#include <set>
#include <cassert>
#include <iomanip>
#include <cstring>
#include <sstream>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair <int, int> pii;
#define F first
#define S second
#define pb push_back
#define sz(x) ((int)(x).size())

inline int in(){ int x, y; y = scanf("%d", &x); return x; }

const int N = 1000 + 5;
const int K = 100 + 5;
const int L = 10 + 5;
const int C = 40;
const int T = N*L;
const int ASCII = 200;
const bool LOG = false;

int mp[ASCII];
char rmp[C];

void preprocess(){
	int cnt = 0;
	for (int c = '0'; c <= '9'; c++){
		mp[c] = cnt;
		rmp[cnt] = c;
		cnt++;
	}
	for (int c = 'A'; c <= 'Z'; c++){
		mp[c] = cnt;
		rmp[cnt] = c;
		cnt++;
	}
	mp['-'] = cnt;
	rmp[cnt] = '-';
	cnt++;
	mp['$'] = cnt;
	rmp[cnt] = '$';
	cnt++;
}

struct Trie{

	int root = -1;
	int node_cnt = 0;
	int nxt[T][C];
	int id2node[N];
	int par[T];
	int par2this[T];
	bool excluding[T];

	int allocate_node(int parent=-1, char c=0){
		int node = node_cnt++;
		par[node] = parent;
		par2this[node] = c;
		excluding[node] = true;
		fill(nxt[node], nxt[node] + C, -1);
		return node;
	}

	int get_root(){
		if (root == -1)
			root = allocate_node();
		return root;
	}

	int get_next(int node, char c){
		c = mp[c];
		if (nxt[node][c] == -1)
			nxt[node][c] = allocate_node(node, c);
		return nxt[node][c];
	}

	int insert(string s){
		int node = get_root();
		for (int i = 0; i < s.length(); i++)
			node = get_next(node, s[i]);
		return node;
	}

	void make(int n, string s[]){
		for (int id = 0; id < n; id++)
			id2node[id] = insert("$" + s[id] + "$");
	}

	int get_excluding_ancestor(int id){
		int node = id2node[id];
		assert(excluding[node]);
		while (par[node] != -1 && excluding[par[node]])
			node = par[node];
		return node;
	}

	void exclude(int id){
		for (int node = id2node[id]; node != -1; node = par[node])
			excluding[node] = false;
	}

	void clear_exclusions(){
		fill(excluding, excluding + node_cnt, true);
	}

	string get_string(int node){
		bool exact_word = (node != root && par[node] != root && rmp[par2this[node]] == '$');
		string res = "";
		if (!exact_word)
			res += '*';
		for (; node != root; node = par[node]){
			char c = rmp[par2this[node]];
			if (c != '$')
				res += c;
		}
		reverse(res.begin(), res.end());
		return res;
	}

	void report(){
		cerr << "Number of nodes = " << node_cnt << endl;
		cerr << "ID	Parent	Par2This" << endl;
		for (int i = 0; i < node_cnt; i++)
			cerr << i << "	" << par[i] << "	" << rmp[par2this[i]] << endl;	
	}

} trie, eirt;


vector <int> g[2*N];
int match[2*N];
bool mark[2*N];

// Code by M.Mahdi

bool dfs (int v){
	mark[v] = true;
	for (int u : g[v]){
		if (match[u] == -1 || (!mark[match[u]] && dfs(match[u]))){
			match[v] = u;
			match[u] = v;
			return true;
		}
	}
	return false;
}

int maximum_matching(int n, int m){
	// 	0, 1, ..., n - 1
	// 	n, n + 1, ..., n + m - 1
	//	if match[i] != -1, then {i, match[i]} is an edge in a maximum matching
	int ret=0;
	fill(match, match + n + m, -1);
	while(true)
	{
		fill(mark, mark + n, false);
		int lret = ret;
		for(int i = 0; i < n; i++)
			if(!mark[i] && match[i] == -1)
				ret += dfs(i);
		if(ret == lret)
			break;
	}
	return ret;
}

vector <int> vertex_cover(int n, int m){
	maximum_matching(n, m);
	vector <int> mvc; 				// 		minimum vertex cover
	for (int i = 0; i < n; i++){
		if (mark[i]){
			if(match[i] != -1)
				mvc.push_back(match[i]);
		}
		else
			mvc.push_back(i);
	}
	return mvc;
}

string s[N];
bool photo[N];

int main(){
	preprocess();
	int n = in(), k = in();
	for (int i = 0; i < n; i++)
		cin >> s[i];

	trie.make(n, s);
	for (int i = 0; i < n; i++)
		reverse(s[i].begin(), s[i].end());
	eirt.make(n, s);
	for (int i = 0; i < n; i++)
		reverse(s[i].begin(), s[i].end());

//	trie.report();
//	eirt.report();

	for (int i = 0; i < k; i++){
		fill(photo, photo + n, false);
		trie.clear_exclusions();
		eirt.clear_exclusions();
		int m = in();
		for (int j = 0; j < m; j++)
			photo[in()-1] = true;
		for (int j = 0; j < n; j++)
			if (!photo[j]){
				trie.exclude(j);
				eirt.exclude(j);
			}
		map <int, int> node2vertex[2];
		map <int, int> vertex2node[2];
		int part_size[2] = {0, 0};
		vector <pii> edges;
		for (int j = 0; j < n; j++)
			if (photo[j]){
				int node[2] = {trie.get_excluding_ancestor(j), eirt.get_excluding_ancestor(j)};
				// cerr << "Trie(" << node[0] << ")	or	Eirt(" << node[1] << ")" << endl;
				for (int part = 0; part < 2; part++)
					if (node2vertex[part].find(node[part]) == node2vertex[part].end()){
						vertex2node[part][part_size[part]] = node[part];
						node2vertex[part][node[part]] = part_size[part];
						part_size[part]++;
					}
				edges.pb({node2vertex[0][node[0]], node2vertex[1][node[1]]});
			}
		int graph_size = part_size[0] + part_size[1];
		for (int v = 0; v < graph_size; v++){
			g[v].clear();
			match[v] = -1;
			mark[v] = false;
		}
		for (auto &e : edges)
			e.S += part_size[0];
		for (auto e : edges){
			g[e.F].pb(e.S);
			g[e.S].pb(e.F);
		}
		vector <int> mvc = vertex_cover(part_size[0], part_size[1]);
		vector <string> wildcards;
		for (auto v : mvc){
			if (v < part_size[0])
				wildcards.pb(trie.get_string(vertex2node[0][v]));
			else{
				string str = eirt.get_string(vertex2node[1][v-part_size[0]]);
				reverse(str.begin(), str.end());
				wildcards.pb(str);
			}
		}
		cout << m*1000 + wildcards.size() << ' ' << wildcards.size();
		for (auto wildcard : wildcards)
			cout << ' ' << wildcard;
		cout << endl;
	}
	return 0;
}

