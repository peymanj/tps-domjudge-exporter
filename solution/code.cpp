#include <bits/stdc++.h>
#pragma GCC optimize ("O2,unroll-loops")
//#pragma GCC optimize("no-stack-protector,fast-math")
//#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")

using namespace std;
typedef long long ll;
typedef long double ld;
typedef pair<int, int> pii;
typedef pair<pii, int> piii;
typedef pair<ll, ll> pll;
#define debug(x) cerr<<#x<<'='<<(x)<<endl;
#define debugp(x) cerr<<#x<<"= {"<<(x.first)<<", "<<(x.second)<<"}"<<endl;
#define debug2(x, y) cerr<<"{"<<#x<<", "<<#y<<"} = {"<<(x)<<", "<<(y)<<"}"<<endl;
#define debugv(v) {cerr<<#v<<" : ";for (auto x:v) cerr<<x<<' ';cerr<<endl;}
#define all(x) x.begin(), x.end()
#define pb push_back
#define SZ(x) ((int) x.size())

const int inf=1000000010;
const ll INF=10000000000000010LL;
const int mod=1000000007;
const int MAXN=1010, LOG=20;

int n, m, k, u, v, x, y, t, a, b, sz;
int A[MAXN], B[MAXN], good[MAXN];
int psA[MAXN], psB[MAXN], bad[MAXN*4];
int PA[MAXN], PB[MAXN];
bool mark[MAXN][MAXN];
int dp[MAXN][MAXN];
piii par[MAXN][MAXN];
string S[MAXN], SR[MAXN], Q[MAXN*4];
vector<pii> VA[MAXN], VB[MAXN];

inline bool cmp1(int i, int j){ return S[i]<S[j];}
inline bool cmp2(int i, int j){ return SR[i]<SR[j];}
inline bool is_pref(string &s, string &t){
	if (SZ(s)>SZ(t)) return 0;
	for (int i=0; i<SZ(s); i++) if (s[i]!=t[i]) return 0;
	return 1;
}
inline void upd(int i, int j, int ii, int jj, int cost, int typ){
	if (dp[i][j]>dp[ii][jj]+cost){
		dp[i][j]=dp[ii][jj]+cost;
		par[i][j]={{ii, jj}, typ};
	}
}
void Solve(){
	for (int i=1; i<=n; i++){
		psA[i]=psA[i-1] + (good[A[i]]==0);
		psB[i]=psB[i-1] + (good[B[i]]==0);
	}
	for (int i=1; i<=n; i++){
		for (pii p:VA[i]) bad[p.second]=(psA[i]-psA[p.first-1]>0);
		for (pii p:VB[i]) bad[p.second]=(psB[i]-psB[p.first-1]>0);
	}
	memset(dp, 63, sizeof(dp));
	memset(par, 0, sizeof(par));
	for (int i=0; i<=n; i++) dp[i][0]=dp[0][i]=0;
	for (int i=1; i<=n; i++) for (int j=1; j<=n; j++){
		if (PB[A[i]]>j) upd(i, j, i-1, j, 0, -3);
		else if (PA[B[j]]>i) upd(i, j, i, j-1, 0, -3);
		else{
			if (good[A[i]]) upd(i, j, i-1, j, good[A[i]], -1);
			if (good[B[j]]) upd(i, j, i, j-1, good[B[j]], -2);
			for (pii p:VA[i]) if (!bad[p.second]) upd(i, j, p.first-1, j, +1, p.second);
			for (pii p:VB[j]) if (!bad[p.second]) upd(i, j, i, p.first-1, +1, p.second); 
		}
	}
	cout<<1000*k+dp[n][n]<<" "<<dp[n][n]<<" ";
	int i=n, j=n;
	while (i && j){
		pii p=par[i][j].first;
		if (par[i][j].second==-1 && good[A[i]]) cout<<S[A[i]]<<" ";
		if (par[i][j].second==-2 && good[B[j]]) cout<<S[B[j]]<<" ";
		if (par[i][j].second>=0) cout<<Q[par[i][j].second]<<" ";
		i=p.first, j=p.second;
	}
	cout<<"\n";
}

int main(){
	ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	cin>>n>>m;
	for (int i=1; i<=n; i++){
		cin>>S[i];
		SR[i]=S[i];
		reverse(all(SR[i]));
		A[i]=B[i]=i;
	}
	sort(A+1, A+n+1, cmp1);
	sort(B+1, B+n+1, cmp2);
//	cerr<<"A: ";for (int i=1; i<=n; i++) cerr<<A[i]<<" \n"[i==n];
//	cerr<<"B: ";for (int i=1; i<=n; i++) cerr<<B[i]<<" \n"[i==n];
	for (int i=1; i<=n; i++) PA[A[i]]=PB[B[i]]=i;
	for (int i=1; i<=n; i++){
		string W;
		for (int j=0; j<SZ(S[A[i]]); j++){
			W+=S[A[i]][j];
			if (!is_pref(W, S[A[i-1]])){
				int r=i;
				while (is_pref(W, S[A[r+1]])) r++; 
				if (!mark[i][r]){
					mark[i][r]=1;
					Q[sz]=W+"*";
					VA[r].pb({i, sz++});
//					cerr<<"VA.pb: "<<i<<" "<<r<<"   "<<Q[sz-1]<<"\n";
				}
			}
		}
	}
	memset(mark, 0, sizeof(mark));
	for (int i=1; i<=n; i++){
		string W;
		for (int j=0; j<SZ(SR[B[i]]); j++){
			W+=SR[B[i]][j];
			if (!is_pref(W, SR[B[i-1]])){
				int r=i;
				while (is_pref(W, SR[B[r+1]])) r++; 
//				if (B[i]==8 && SZ(W)==2) debug2(i, r)
				if (!mark[i][r]){
					mark[i][r]=1;
					Q[sz]=W+"*";
					reverse(all(Q[sz]));
					VB[r].pb({i, sz++});
//					cerr<<"VB.pb: "<<i<<" "<<r<<"   "<<sz<<" "<<Q[sz-1]<<"\n";
				}
			}
		}
	}
	while (m--){
		memset(good, 0, sizeof(good));
		cin>>k;
		for (int i=0; i<k; i++) cin>>x, good[x]=1;
		if (k==n) cout<<1000*k+1<<" 1 *\n";
		else if (k==0) cout<<"0 0\n";
		else Solve();
	}
	
	return 0;
}
/*
8 7
K-PAX SIRIUS REGULUS ARCTURUS BELLATRIX ANDROMEDA CYGNUS SCORPIUS
8 1 2 3 4 5 6 8 7
6 2 3 4 7 8 6
5 1 2 3 5 8
3 5 6 7
2 2 8
0
1 1

8 1
K-PAX SIRIUS REGULUS ARCTURUS BELLATRIX ANDROMEDA CYGNUS SCORPIUS
6 2 3 4 6 7 8


*/

