#include<bits/stdc++.h>

#define F first
#define S second
#define PB push_back
#define sz(s) (int(s.size()))
#define bit(n, k) (((n)>>(k))&1)

using namespace std;

typedef pair<int, int> pii;
typedef long long ll;

const int inf = 1e9 + 10;
const int maxn = 1e4 + 10, maxn2 = 1e3 + 10, alpha = 37;


template<typename A, typename B> ostream& operator << (ostream& out, pair<A, B> p){
    out << "{" << p.first << ", " << p.second << "}";
    return out;
}
template<typename T> ostream& operator << (ostream& out, vector<T> v){
    out << "[";
    for(int i = 0; i < (int)v.size(); i++){
	out << v[i];
	if(i+1 != (int)v.size())
	    out << ", ";
    }
    out << "]";
    return out;
}

struct Trie{

    int toC(char c){
	if('A' <= c && c <= 'Z')
	    return c - 'A';
	if('0' <= c && c <= '9')
	    return 26 + c - '0';
	if(c == '-')
	    return 36;
	assert(0);
    }
    
    string STR[maxn];
    int nxt[maxn][alpha], par[maxn], C;
    vector<int> ids[maxn];
    bool arr[maxn2];
    bool bad[maxn];
    vector<int> vec;    
    vector<pair<string, vector<int> >> ret;
    
    Trie(){
	C = 0;
	memset(nxt, 0, sizeof nxt);
    }
    void add(string s, int id){
	int tmp = 0;
	for(char c : s){
	    if(nxt[tmp][toC(c)] == 0){
		nxt[tmp][toC(c)] = ++C;
		STR[nxt[tmp][toC(c)]] = STR[tmp] + c;
	    }
	    tmp = nxt[tmp][toC(c)];
	}
	ids[tmp].PB(id);
    }
    void go_all(int u){
	for(int x : ids[u])
	    vec.PB(x);
	for(int i = 0; i < alpha; i++){
	    if(nxt[u][i] != 0)
		go_all(nxt[u][i]);
	}
    }
    void put(int u){
	vec.clear();
	go_all(u);
	if(!vec.empty())
	    ret.PB({STR[u] + "*", vec});
    }
    vector< pair<string, vector<int>> > verts(vector<int> ids){
	ret.clear();
	memset(arr, 0, sizeof arr);
	memset(bad, 0, sizeof bad);
	for(int x : ids)
	    arr[x] = 1;
	dfs(0);
	if(bad[0] == 0){
	    put(0);
	}
	else{
	    for(int i = 1; i < C; i++){
		if(bad[i] == 0 && bad[par[i]]){
		    put(i);
		}
	    }
	}
	return ret;
    }
    void dfs(int u){
	for(int id : ids[u])
	    bad[u]|= !arr[id];
	for(int i = 0; i < alpha; i++){
	    if(nxt[u][i] != 0){
		par[nxt[u][i]] = u;
		dfs(nxt[u][i]);
		bad[u]|= bad[nxt[u][i]];
	    }
	}
    }
};

template<typename T,T inf,int MAXN> struct Flow{
    vector< pair<int,T> >v[MAXN];
    vector<int>idv[MAXN];
    int h[MAXN],arr[MAXN],pt[MAXN];
    T lim;

    bool bfs(int Source,int Sink){
	memset(h,0,sizeof h);
	int L=0,R=0;
	arr[R++]=Source;
	h[Source]=1;
	while(L<R){
	    int u=arr[L++];
	    if(u==Sink) return 1;
	    for(pair<int,T> p:v[u]){
		if(p.S>=lim && h[p.F]==0)
		    h[p.F]=h[u]+1,arr[R++]=p.F;
	    }
	}
	return 0;
    }
    T dfs(int u,int Sink,T flow){// flow > 0
	if(u==Sink) return flow;
	T ans=0;
	while(pt[u]<sz(v[u])){
	    int &y=v[u][pt[u]].F;
	    T &w=v[u][pt[u]].S,&w2=v[y][idv[u][pt[u]]].S;
	    if(h[u]+1==h[y] && w>=lim){
		T num=dfs(y,Sink,min(flow-ans,w));
		ans+=num;w-=num;w2+=num;
	    }
	    if(flow==ans) break;
	    ++pt[u];
	}
	return ans;
    }
    void add_edge(int a,int b,T wa,T wb=0){
	idv[a].PB(sz(v[b]));
	idv[b].PB(sz(v[a]));
	v[a].PB({b,wa});
	v[b].PB({a,wb});
    }
    T flow(int Source,int Sink,int MXB=0){// MXB(is power of two) for scaling flow
	T ans=0;
	for(int i=MXB;i>=0;i--){
	    lim=(1ll<<i);
	    while(bfs(Source,Sink)){
		memset(pt,0,sizeof pt);
		ans+=dfs(Source,Sink,inf);
	    }
	}
	return ans;
    }
    
    bool _mark[MAXN];
    bool down[MAXN], up[MAXN];
    void watch(int u, int lss){
	if(u >= lss)
	    up[u] = 1;
	_mark[u] = 1;
	for(pair<int, T> p : v[u]){
	    int y = p.F, ww = p.S;
	    if(ww > 0 && !_mark[y])
		watch(y, lss);
	}
    }
    vector<int> cover(int Source, int Sink, int lss){
	memset(_mark, 0, sizeof _mark);
	memset(down, 0, sizeof down);
	memset(up, 0, sizeof up);
	watch(Source, lss);
	
	for(pair<int, T> p : v[Source]){
	    if(p.S == 0 && p.F != Source && p.F != Sink)
		down[p.F] = 1;
	}
	for(int i = lss; i < MAXN; i++){
	    if(up[i] && i != Source && i != Sink){
		for(pair<int, T> p : v[i]){
		    if(p.S == 1 && p.F != Source && p.F != Sink)
			down[p.F]^= 1;
		}
	    }
	}
	vector<int> ret;
	for(int i = 0; i <= Sink; i++){
	    if(i != Source && i != Sink){
		if(up[i] || down[i]){
		    ret.PB(i);
		}
	    }
	    //	    cout << i << "  " << up[i] << " " << down[i] << endl;
	}
	return ret;
    }
    
    void clear(){
	for(int i = 0; i < MAXN; i++){
	    v[i].clear();
	    idv[i].clear();
	}	
    }
};Flow<int, inf, 3000> flw;

Trie tr, trr;

bool in[maxn2];
int who[maxn2][2];
bool must[maxn2][2];
string _s[maxn2];

string rev(string s){
    reverse(s.begin(), s.end());
    return s;
}

int main(){
    ios_base::sync_with_stdio(0), cin.tie(), cout.tie();

    int n, q;
    cin >> n >> q;
    for(int i = 0; i < n; i++){
	string s;
	cin >> s;
	_s[i] = s;
	tr.add(s, i);
	reverse(s.begin(), s.end());
	trr.add(s, i);
    }
    while(q--){
	int k;
	cin >> k;
	vector<int> ids(k);
	for(int i = 0; i < k; i++){
	    cin >> ids[i];
	    --ids[i];
	}
	vector< pair<string, vector<int> > > etr = tr.verts(ids), etrr = trr.verts(ids);
	vector<string> ans;

	/////////
	//	cout << etr << endl << etrr << endl;
	//	cout << "SALAM " << endl;
	
	memset(in , 0, sizeof in);
	memset(who, -1, sizeof who);
	memset(must, 0, sizeof must);
	flw.clear();
	
	for(int x : ids)
	    in[x] = 1;
	int ID = 0;
	for(auto it : etr){
	    for(int x : it.S)
		who[x][0] = ID;
	    ID++;
	}
	ID = 0;
	for(auto it : etrr){
	    for(int x : it.S)
		who[x][1] = ID;
	    ID++;
	}

	int N0 = sz(etr), N = sz(etr) + sz(etrr) + 2, SRC = N-2, SNK = N-1;
	
	for(int i = 0; i < n; i++){
	    if(in[i]){
		if(who[i][0] != -1 && must[who[i][0]][0])
		    continue;
		if(who[i][1] != -1 && must[who[i][1]][1])
		    continue;
		if(who[i][0] == -1 && who[i][1] == -1){
		    //		    cout << "HALAT1 " << i << endl;
		    ans.PB(_s[i]);
		    continue;
		}
		if(who[i][0] != -1 && who[i][1] == -1){
		    // cout << "HALAT2 " << i << endl;

		    ans.PB(etr[who[i][0]].F);
		    must[who[i][0]][0] = 1;
		    continue;
		}
		if(who[i][0] == -1 && who[i][1] != -1){
		    // cout << "HALAT3 " << i << endl;
		    ans.PB(rev(etrr[who[i][1]].F));
		    must[who[i][1]][1] = 1;
		    continue;
		}
		if(who[i][0] != -1 && who[i][1] != -1){
		    // cout << "HALAT4 " << i << endl;
		    // cout << who[i][0] << "       " << N0 + who[i][1] << endl;
		    flw.add_edge(who[i][0], N0 + who[i][1], 1);
		}
	    }
	}
	
	for(int i = 0; i < sz(etr); i++)
	    flw.add_edge(SRC, i, 1);
	for(int i = 0; i < sz(etrr); i++)
	    flw.add_edge(i + N0, SNK, 1);

	flw.flow(SRC, SNK);
	vector<int> cover = flw.cover(SRC, SNK, N0);
	
	for(int x : cover){
	    if(x < N0)
		ans.PB(etr[x].F);
	    else
		ans.PB(rev(etrr[x-N0].F));
	}

	cout << 1000 * k + sz(ans) << " " << sz(ans) << " ";
	for(string st : ans){
	    cout << st << " ";
	}
	cout << "\n";
    }
    return 0;
}
